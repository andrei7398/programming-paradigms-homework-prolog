
:-ensure_loaded('probleme.pl').
:-ensure_loaded('testing.pl').

list_empty([]).

get_days(Context, Days) :- member(T, Context), T = days(Days).

get_times(Context, Times) :- member(T, Context), T = times(Times).

get_rooms(Context, Rooms) :- member(T, Context), T = rooms(Rooms). 

get_groups(Context, Groups) :- member(T, Context), T = groups(Groups). 

get_activities(Context, Activities) :- member(T, Context), T = activities(Activities). 

get_staff(Context, Staff) :- member(T, Context), T = staff(Staff). 

get_all(Context, Days, Times, Rooms, Groups, Activities, Staff) :- 
	get_days(Context, Days),
	get_times(Context, Times),
	get_rooms(Context, Rooms),
	get_groups(Context, Groups),
	get_activities(Context, Activities),
	get_staff(Context, Staff).
  
 
% schedule(+Context, -Sol)
% pentru contextul descris, întoarce o soluție care respectă
% constrângerile fizice și de curiculă.

make_slot(D,T,R,G,A,S,Slot) :- Slot = [D,T,R,G,A,S].


make_combination_rooms(D,T,R,Comb) :- Comb = [D,T,R].


make_combination_teachers(G,A_Name,S_Name,Teacher) :- Teacher = [G,A_Name,S_Name].


kind_of_loop(0,_,_,_,[]).
kind_of_loop(N,G,A_Name,S_Name,[Teacher|Sol]) :- 
	\+ N == 0, 
	make_combination_teachers(G,A_Name,S_Name,Teacher), 
	N2 is N-1, 
	kind_of_loop(N2,G,A_Name,S_Name,Sol). 


room_ok(E1, E2) :-
	[D, T, R, _, _, _] = E1,
	[D2, T2, R2, _, _, _] = E2,
	(D \== D2; T \== T2; R \== R2).


staff_ok(E1, E2) :-
	[D, T, _, _, _, S_Name] = E1,
	[D2, T2, _, _, _, S2_Name] = E2,
	(D \== D2; T \== T2; S_Name \== S2_Name).


group_ok(E1, E2) :-
	[D, T, _, G, _, _] = E1,
	[D2, T2, _, G2, _, _] = E2,
	(G \== G2; D \== D2; T \== T2).


count_hours(_,[],[]).
count_hours([G,A_Name,A_Hours],[L|Ls],[H2|Hours]) :- 
	[_,(_-_,H2),_,G2,A_Name2,_] = L,
	(G == G2, A_Name == A_Name2),
	count_hours([G,A_Name,A_Hours],Ls,Hours).
count_hours([G,A_Name,A_Hours],[L|Ls],Hours) :- 
	[_,(_-_,_),_,G2,A_Name2,_] = L,
	(G \= G2; A_Name \= A_Name2),
	count_hours([G,A_Name,A_Hours],Ls,Hours).


number_of_hours([], 0).
number_of_hours([H|T], Hours) :- number_of_hours(T, Rest), Hours is H + Rest.


get_hours([G,A_Name,A_Hours],[L|Ls],Hours) :- 
	count_hours([G,A_Name,A_Hours],[L|Ls],List_Hours), 
	number_of_hours(List_Hours, Hours).


search_activities(_,[],0).
search_activities(A_Name,[A|_],H) :-
	(X,Y) = A,
	A_Name == X,
	H is Y.
search_activities(A_Name,[A|Rest],H) :-
	(X,_) = A,
	A_Name \= X,
	search_activities(A_Name,Rest,H).


insertUniq(P,[],[P]).
insertUniq(P,[L|Ls],[L|Ls]) :- 
	(\+ room_ok(P,L); \+ staff_ok(P,L); \+ group_ok(P,L)), !.
insertUniq(P,[L|Ls],[L|O]) :- insertUniq(P,Ls,O).


loop_insertUniq(_,[],P,P).
loop_insertUniq(Activities,[P|Rest],L,Sol) :- 
	[_,_,_,G,A_Name,_] = P,
	search_activities(A_Name,Activities,Need),
	get_hours([G,A_Name,Need],L,Hours),
	Need > Hours,
	insertUniq(P,L,New_L), 
	loop_insertUniq(Activities,Rest,New_L,Sol).


solution_verify(Groups,Activities,Solution) :- 
	member(G,Groups),
	member(A,Activities),
	(A_Name,Need) = A,
	get_hours([G,A_Name,Need],Solution,Hours),
	Need =< Hours.


%schedule(Context, []) :-
%	get_activities(Context, Activities),
%	list_empty(Activities).
	

schedule(Context, Schedule) :-
	get_all(Context, Days, Times, Rooms, Groups, Activities, Staff),
	findall(Sol,(member(D, Days),
		member(T, Times),
		member(R, Rooms),
		member(G, Groups),
		member(A, Activities), 
		member(S, Staff),
		(S_Name,S_Activities) = S,
		(A_Name,_) = A,
		member(A_Name,S_Activities),
		make_slot(D,T,R,G,A_Name,S_Name,Sol)),
		Solutions),
	loop_insertUniq(Activities,Solutions,[],Schedule),
	solution_verify(Groups,Activities,Schedule).


schedule(Context, []) :-
	get_all(Context, Days, Times, Rooms, Groups, Activities, Staff),
	findall(Sol,(member(D, Days),
		member(T, Times),
		member(R, Rooms),
		member(G, Groups),
		member(A, Activities), 
		member(S, Staff),
		(S_Name,S_Activities) = S,
		(A_Name,_) = A,
		member(A_Name,S_Activities),
		make_slot(D,T,R,G,A_Name,S_Name,Sol)),
		Solutions),
	loop_insertUniq(Activities,Solutions,[],Schedule),	
	\+ solution_verify(Groups,Activities,Schedule).


% cost(+Sol, -Cost)
% pentru soluția dată, întoarce costul implicat de constrângerile de
% preferință care au fost încălcate.
cost(_, _) :- fail.

% schedule_best(+Context, -Sol, -Cost)
% pentru contextul descris, întoarce soluția validă cu cel mai bun (cel
% mai mic) cost (sau una dintre ele, dacă există mai multe cu același
% cost)
schedule_best(_, _, _) :- fail.













